/************************************************************************************************************************
@Name: APTPS_Approvals_Lease
@Author: Conga PS Dev Team
@CreateDate: 29 October 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
public class APTPS_Approvals_Lease implements APTPS_ApprovalUsersInterface{
   public void checkLegalApprovedRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){}
   
   public void checkDDCApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
       for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {
           //Commented for Hours 03.11
            /*if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) == 25) {                        
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                
                break;                                        
            }*/
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 6)) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;
            }
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c != '60')) {
                proposalObj.APTPS_Requires_DDC_Approval__c = true;
                //APTPS_Contract_Length__c
                //proposalObj.APTPS_Contract_Length__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length__c);
                break;
            }
        }
        if(proposalObj.APTPS_Requires_DDC_Approval__c){
               proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c = false;
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }
   }
    
    public void checkSalesOpsApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Lease NJA START*/
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) > 3) && (Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <=6) ) {                        
                proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c  = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            /*For Lease NJA END*/
           if(proposalObj.APTS_Requires_VP_Sales_Ops_Approval__c ){
               proposalObj.APTS_Requires_Sales_Director_Approval__c = false;
           }

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesDirectorApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            /*For Share NJA START*/
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c !=null &&(Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c) <= 3) ) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Months__c = Integer.valueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c);
                break;                                        
            }
            system.debug('DELAYED START END DATE ------'+proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c);
            if (APTS_Constants.NJUS_LEASE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c != null ) {                        
                system.debug('inside DELAYED START END DATE');
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Delayed_Start_Date__c = proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Date__c;
                break;                                        
            }
            if (APTS_ConstantUtil.NJA_LEASE_CODE.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equals(proposalLineItem.Apttus_QPConfig__OptionId__r.ProductCode) && Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c) > 25) {                        
                proposalObj.APTS_Requires_Sales_Director_Approval__c = true;
                //proposalObj.APTPS_Hours__c = Integer.ValueOf(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c);
                break;                                        
            }
            /*For Share NJA END*/
           

        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkDemoTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {}
    
    public void checkDirectorARApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        system.debug('inside checkDirectorARApprovalRequired');
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJUS_LEASE .equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && APTS_Constants.DISCOUNT_AMOUNT.equals(proposalLineItem.Apttus_QPConfig__AdjustmentType__c)&& proposalLineItem.Apttus_QPConfig__AdjustmentAmount__c > 0) {                        
                proposalObj.APTPS_Requires_Director_AR_Approval__c = true;
                //proposalObj.APTPS_Operating_Fund_Discount_Applied__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkRVPApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode) && (APTS_Constants.PREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c) || APTS_Constants.NONPREMIUM.equals(proposalLineItem.Apttus_QPConfig__AttributeValueId__r.APTPS_NJE_Rate_Type__c))) {                        
                proposalObj.APTS_Requires_RVP_Approval__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);
    }
    
    public void checkSalesOpsTeamApprovalRequired(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) { 
            if (APTS_Constants.NJE_DEMO.equals(proposalLineItem.Apttus_Proposal__Product__r.ProductCode)) {                        
                proposalObj.APTPS_Requires_Sales_Ops_Team_Approval__c = true;
                break;                                        
            }
        }
         system.debug('proposalObj-->'+proposalObj);        
    }
    
    
    
    
    /** 
    @description: Lease NJE implementation
    @param:
    @return: 
    */
    public class NJE_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJE dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Demo obj = new APTPS_Approvals_Demo();
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkRVPApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsTeamApprovalRequired(proposalObj,quoteLines);
            
        }
    }
    
    /** 
    @description: Lease NJA implementation
    @param:
    @return: 
    */
    public class NJA_Approvals implements APTPS_ApprovalsInterface {
        public void updateApprovals(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJA dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_Approvals_Lease obj = new APTPS_Approvals_Lease();
            obj.checkDirectorARApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesDirectorApprovalRequired(proposalObj,quoteLines);
            obj.checkSalesOpsApprovalRequired(proposalObj,quoteLines);
            obj.checkDDCApprovalRequired(proposalObj,quoteLines);
            
        }
    }
}