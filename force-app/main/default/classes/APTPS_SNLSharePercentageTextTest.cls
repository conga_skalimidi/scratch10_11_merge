/************************************************************************************************************************
@Author: Conga PS Dev Team
@CreateDate: 08 Sept 2021
@Description: 
************************************************************************************************************************/
@isTest
public class APTPS_SNLSharePercentageTextTest {
    @isTest
    static void testPopulateAgreementExtFields(){
        Test.startTest();
        //APTPS_SNLSharePercentageText sharePer = new APTPS_SNLSharePercentageText();
        String res = APTPS_SNLSharePercentageText.populateAgreementExtFields('3.125');
        System.assertEquals('a three and one-eighth',res);
        System.assertNotEquals(null,res);
        Test.stopTest();
    }
    @isTest
    static void testPopulateAgreementExtFields2(){
        Test.startTest();
        //APTPS_SNLSharePercentageText sharePer = new APTPS_SNLSharePercentageText();
        String res = APTPS_SNLSharePercentageText.populateAgreementExtFields('600');
        System.assertEquals(null,res);
        Test.stopTest();
    }
}