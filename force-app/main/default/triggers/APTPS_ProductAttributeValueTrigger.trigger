trigger APTPS_ProductAttributeValueTrigger on Apttus_Config2__ProductAttributeValue__c (before insert) {
    system.debug('>>>>>>>>>>Before dispatching to Product Attribute Value trigger handler');
    TriggerHandlerDispatcher.execute(Apttus_Config2__ProductAttributeValue__c.getSObjectType());
}